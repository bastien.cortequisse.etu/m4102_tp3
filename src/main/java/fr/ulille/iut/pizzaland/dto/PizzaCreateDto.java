package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

public class PizzaCreateDto {
	private String name;
	private List<UUID> list;
	
	public PizzaCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}


	public List<UUID> getList() {
		return list;
	}

	public void setList(List<UUID> list) {
		this.list = list;
	}
	
	
}
