package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

public class PizzaDto {
	private UUID id;
    private String name;
    private List<UUID> list;
    
	public PizzaDto() {
	}
    
	public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

	public List<UUID> getIngred() {
		return list;
	}

	public void setIngred(List<UUID> ingred) {
		this.list = ingred;
	}
    
    
}
