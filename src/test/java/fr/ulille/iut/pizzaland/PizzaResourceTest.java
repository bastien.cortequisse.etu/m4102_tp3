package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


public class PizzaResourceTest extends JerseyTest{
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private PizzaDao dao;
    private IngredientDao ingDao;
    
    @Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();

        return new ApiV1();

    }
    
    @Before
    public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createTableAndIngredientAssociation();
      
      ingDao = BDDFactory.buildDao(IngredientDao.class);
      ingDao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
       dao.dropAssociationTable();
       ingDao.dropTable();
       dao.dropPizzaTable();
    }
    
    @Test
    public void testGetEmptyList() {
        Response response = target("/pizza").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<PizzaDto> ingredients;
        ingredients = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, ingredients.size());

    }

    @Test
    public void testGetExistingPizza() {

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        dao.insert(pizza);

        Response response = target("/pizza").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza, result);
    }
    
    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizza").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testCreatePizza() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("Chorizo");
        pizzaCreateDto.setList(new ArrayList<UUID>());

        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizza/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }
    
    @Test
    public void testCreateSamePizza() {
        Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setList(new ArrayList<Ingredient>());
        dao.insert(pizza);
        
        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testDeleteExistingPizza() {
    	Pizza pizza = new Pizza();
    	pizza.setName("Chorizo");
    	dao.insert(pizza);

    	Response response = target("/pizza/").path(pizza.getId().toString()).request().delete();

    	assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

    	Pizza result = dao.findById(pizza.getId());
    	assertEquals(result, null);
    }
    
    @Test
    public void testDeleteNotExistingPizza() {
      Response response = target("/pizza").path(UUID.randomUUID().toString()).request().delete();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testGetPizzaName() {
    	Pizza pizza = new Pizza();
      pizza.setName("Chorizo");
      dao.insert(pizza);

      Response response = target("pizza").path(pizza.getId().toString()).path("name").request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      assertEquals("Chorizo", response.readEntity(String.class));
   }

   @Test
   public void testGetNotExistingPizzaName() {
     Response response = target("pizza").path(UUID.randomUUID().toString()).path("name").request().get();

     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }

   @Test
   public void testCreateWithForm() {
       Form form = new Form();
       form.param("name", "chorizo");

       Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
       Response response = target("pizza").request().post(formEntity);

       assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
       String location = response.getHeaderString("Location");
       String id = location.substring(location.lastIndexOf('/') + 1);
       Pizza result = dao.findById(UUID.fromString(id));

       assertNotNull(result);
   }
   
   //avec ingredient
   
   /*@Test TODO
   public void testGetExistingPizzaIngredient() {
   	Pizza pizza = new Pizza();
       pizza.setName("Chorizo");
       pizza.setList(new ArrayList<Ingredient>());
       
       Ingredient in1=new Ingredient();
       in1.setName("Fromage");
       Ingredient in2=new Ingredient();
       in2.setName("Tomate");
       pizza.getList().add(in1);
       pizza.getList().add(in2);

       ingDao.insert(in1);
       ingDao.insert(in2);
       
       dao.insertPizzaWithIngredient(pizza);

       Response response = target("/pizza").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

       assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

       Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
       assertEquals(pizza, result);
   }*/
   
   
   @Test
   public void testCreatePizzaIngredient() {
       PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
       pizzaCreateDto.setName("Chorizo");
       pizzaCreateDto.setList(new ArrayList<UUID>());
       Ingredient in1=new Ingredient();
       in1.setName("Poivron");
       Ingredient in2=new Ingredient();
       in2.setName("Piment");

       pizzaCreateDto.getList().add(in1.getId());
       pizzaCreateDto.getList().add(in2.getId());

       ingDao.insert(in1);
       ingDao.insert(in2);
       
       Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

       assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

       PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

       assertEquals(target("/pizza/" + returnedEntity.getId()).getUri(), response.getLocation());
       assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
   }
   
   @Test
   public void testCreateSamePizzaIngredient() {
       Pizza pizza = new Pizza();
       pizza.setName("Chorizo");
       pizza.setList(new ArrayList<>());
       
       Ingredient in1=new Ingredient();
       in1.setName("testSamePizza1");
       Ingredient in2=new Ingredient();
       in2.setName("testSamePizza2");

       pizza.getList().add(in1);
       pizza.getList().add(in2);

       ingDao.insert(in1);
       ingDao.insert(in2);
       
       dao.insertPizzaWithIngredient(pizza);
       
       PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
       Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

       assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
   }

   
   @Test
   public void testDeleteExistingPizzaIngredient() {
   	Pizza pizza = new Pizza();
   	pizza.setName("Chorizo");
   	Ingredient in1=new Ingredient();
    in1.setName("testDelete1");
    Ingredient in2=new Ingredient();
    in2.setName("testDelete2");

    pizza.getList().add(in1);
    pizza.getList().add(in2);

    ingDao.insert(in1);
    ingDao.insert(in2);
    
   	dao.insertPizzaWithIngredient(pizza);

   	Response response = target("/pizza/").path(pizza.getId().toString()).request().delete();

   	assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

   	Pizza result = dao.findById(pizza.getId());
   	assertEquals(result, null);
   }
   
   
   @Test
   public void testGetPizzaIngredientName() {
   	Pizza pizza = new Pizza();
     pizza.setName("Chorizo");
     pizza.setList(new ArrayList<>());
     
     Ingredient in1=new Ingredient();
     in1.setName("testName1");
     Ingredient in2=new Ingredient();
     in2.setName("testName2");

     pizza.getList().add(in1);
     pizza.getList().add(in2);

     ingDao.insert(in1);
     ingDao.insert(in2);
     
     dao.insertPizzaWithIngredient(pizza);

     Response response = target("pizza").path(pizza.getId().toString()).path("name").request().get();

     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

     assertEquals("Chorizo", response.readEntity(String.class));
  }



  /*@Test
  public void testCreateWithFormIngredient() {//TODO
      Form form = new Form();
      form.param("name", "chorizo");

      Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
      Response response = target("pizza").request().post(formEntity);

      assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
      String location = response.getHeaderString("Location");
      String id = location.substring(location.lastIndexOf('/') + 1);
      Pizza result = dao.findById(UUID.fromString(id));

      assertNotNull(result);
  }*/
}
