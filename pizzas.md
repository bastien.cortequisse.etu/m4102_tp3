| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizza                   | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I2)                                                |
| /pizza/{id}              | GET         | <-application/json<br><-application/xml                      |                 | une pizza (I2) ou 404                                                |
| /pizza/{id}/name         | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                            |
| /pizza                   | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (I1) 	  | Nouvella pizza (I2)<br>409 si la pizza existe déjà (même nom) 		 |
| /pizza/{id}        	   | DELETE      |                                                              |                 |                                                                      |



